#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double principal,interest_rate,number_of_years,result;

    cout << "Enter savings amount: ";
    cin >> principal;
    cout << endl;
    
    cout << "Enter the interest_rate: ";
    cin >> interest_rate;
    cout << endl;

    cout << "The number of years: ";
    cin >> number_of_years;
    cout << endl;

    // Compound Interest Formula = (1+i)^n - P
    // P stands for principal; i stands for interest; n stands for the number of compounding periods.

    interest_rate = interest_rate / 100;

    result = principal * pow ((1+interest_rate), number_of_years) - principal;

    cout << "The compound interest is: " << result << endl;
}